package com.example.kamran.bluewhite;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class main extends AppCompatActivity {
    TextView sin;
    LinearLayout circle;
    Intent it;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        circle = (LinearLayout)findViewById(R.id.circle);
        sin = (TextView)findViewById(R.id.sin);

        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(main.this,signup.class);
                finish();
            startActivity(it);

            }
        });
        sin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(main.this,signin.class);
                finish();
                startActivity(it);
            }
        });

    }

    public void onBackPressed(){
        finish();
        it = new Intent(main.this, welcome.class);
        startActivity(it);
    }
}
